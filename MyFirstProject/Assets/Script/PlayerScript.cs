﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public GameObject prefabBullet;

    public float FireRate;
    private float lastFired;

    float cubeSpeed = 5;


    // Use this for initialization
    void Start ()
    {
	   
	}

    // Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //this.transform.position = new Vector3(0, 1, 0);
            this.transform.Translate(Vector3.up * cubeSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            //this.transform.position = new Vector3(0, 1, 0);
            this.transform.Translate(Vector3.down * cubeSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            //this.transform.position = new Vector3(0, 1, 0);
            this.transform.Translate(Vector3.right * cubeSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //this.transform.position = new Vector3(0, 1, 0);
            this.transform.Translate(Vector3.left * cubeSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if(lastFired <= 0)
            {
                Instantiate(prefabBullet, this.transform.position, Quaternion.identity);
                lastFired = FireRate;
            }
        }

        if(lastFired > 0)
        {
            lastFired -= Time.deltaTime;
        }

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    Instantiate(prefabBullet, this.transform.position, Quaternion.identity);
        //    Destroy(objBullet, 2);
        //}
    }
}
