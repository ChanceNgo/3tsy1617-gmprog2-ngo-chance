﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject Prefab;
    public float SpawnDelay;
    public int MaxSpawned;

    public float YPos;
    public Vector2 XRangePos;

    private float currentTime;
    private List<GameObject> spawnedPrefab;

	// Use this for initialization
	void Start () {
        spawnedPrefab = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {

		if(spawnedPrefab.Count < MaxSpawned)
        {
            if(currentTime <= 0)
            {
                GameObject tmp = (GameObject)Instantiate(Prefab, new Vector3(Random.Range(XRangePos.x, XRangePos.y), YPos, 0.0f), Quaternion.identity);
                spawnedPrefab.Add(tmp);
                currentTime = SpawnDelay;
            }
        }

        if(currentTime > 0)
        {
            currentTime -= Time.deltaTime;
        }

        for(int i = 0; i < spawnedPrefab.Count; i++)
        {
            if(spawnedPrefab[i] == null)
            {
                spawnedPrefab.Remove(spawnedPrefab[i]);
                break;
            }
        }

	}
}
