﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour {

    public float DelayTime;
    private float currentTime;

	// Use this for initialization
	void Start () {
        currentTime = DelayTime;
	}
	
	// Update is called once per frame
	void Update () {
		
        if(currentTime <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            currentTime -= Time.deltaTime;
        }

	}
}
