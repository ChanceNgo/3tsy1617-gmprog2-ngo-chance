﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour {

    public int Lives;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Asteroid")
        {
            if(Lives > 0)
                Lives--;
            //{
            //    Destroy(other.tag != "Player");
            //}
        }
    }

}
