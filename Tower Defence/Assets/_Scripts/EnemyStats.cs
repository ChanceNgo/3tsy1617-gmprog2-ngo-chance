﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public int HP;
    public int MaxHP;
    public int GoldReward;

	// Use this for initialization
	void Start () {
        MaxHP = HP;
	}
	
	// Update is called once per frame
	void Update () {
        IsDead();
	}

    void IsDead()
    {
        if (HP <= 0)
        {
            Destroy(gameObject);
            GameObject.Find("GameManager").GetComponent<PlayerStats>().AddGold(GoldReward);
        }
    }

    public void ReceiveDamage(int val)
    {
        HP -= val;
        if (HP < 0)
            HP = 0;
    }

    public void ReceivePercentDamage(int percent)
    {
        float dmgVal = HP * percent/100;
        Debug.Log(dmgVal);
        HP -= Mathf.RoundToInt(dmgVal);
        if (HP < 0)
            HP = 0;

    }
}
