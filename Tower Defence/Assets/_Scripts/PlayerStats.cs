﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public int Gold;
    public Text GoldUI;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GoldUI.text = Gold.ToString();
	}

    public void DeductGold(int val)
    {
        Gold -= val;
        if (Gold < 0)
            Gold = 0;
    }

    public void AddGold(int val)
    {
        Gold += val;
    }
}
