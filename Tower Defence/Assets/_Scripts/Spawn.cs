﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject Asset/Spawn", fileName = "Spawn")]
public class Spawn : ScriptableObject
{
    public GameObject SpawnPrefab;
    public string SpawnName;
    public int SpawnCount;
    public int SpawnHP;
    public int GoldReward;
}
