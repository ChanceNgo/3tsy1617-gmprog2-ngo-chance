﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorTargetting : MonoBehaviour {

    public List<GameObject> TargetsToHit;
    public LayerMask Mask;

	// Use this for initialization
	void Start () {
        TargetsToHit = new List<GameObject>();
	}

    private void Update()
    {
        //Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //transform.position = mousePos;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 100, Mask))
        {
            if(hit.collider != GetComponent<Collider>())
                transform.position = hit.point;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
            TargetsToHit.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
            TargetsToHit.Remove(TargetsToHit.Find(x => x == other.gameObject));
    }

    private void OnDisable()
    {
        TargetsToHit = new List<GameObject>();
    }
}
