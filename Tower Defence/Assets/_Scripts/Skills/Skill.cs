﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour {

    public float CooldownDuration;
    public bool IsCooldown;

    protected float curTime;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		if(IsCooldown)
        {
            curTime -= Time.deltaTime;
            if(curTime <= 0)
            {
                IsCooldown = false;
            }
        }
	}

    public void Cooldown()
    {
        IsCooldown = true;
        curTime = CooldownDuration;
    }

    public virtual void UseSkill()
    {

    }
}
