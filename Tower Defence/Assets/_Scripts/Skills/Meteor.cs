﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Skill
{

    public GameObject TargettingCircle;
    [Range(1, 100)]
    public int Damage;

    private bool isSelecting;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        UseSkill();
    }

    public override void UseSkill()
    {
        if (isSelecting && Input.GetMouseButtonDown(0))
        {
            if (TargettingCircle.GetComponent<MeteorTargetting>())
            {
                isSelecting = false;
                IsCooldown = true;
                List<GameObject> targets = TargettingCircle.GetComponent<MeteorTargetting>().TargetsToHit;
                for (int i = 0; i < targets.Count; i++)
                {
                    targets[i].GetComponent<EnemyStats>().ReceivePercentDamage(Damage);
                }
                TargettingCircle.SetActive(false);
            }
        }
    }

    public void SelectTarget()
    {
        if (!isSelecting)
        {
            isSelecting = true;
            TargettingCircle.SetActive(true);
        }
    }
}
