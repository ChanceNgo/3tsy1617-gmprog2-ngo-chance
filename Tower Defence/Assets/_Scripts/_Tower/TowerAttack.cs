﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum TowerFireRate
{
    Slow,
    Medium,
    Fast,
    VeryFast,
    Null
}

public enum Targetting
{
    LowestHP,
    First,
    Last
}

public class TowerAttack : MonoBehaviour
{
    //Random Damage between two values
    public Vector2 Damage;
    //Enemy to attack
    public GameObject DetectedEnemy;
    //Projectile used by the tower
    public GameObject Projectile;

    //Enemy types that the tower can target
    public List<Type> TargetableType;
    //Fire rate of the tower (based on slow, medium, fast, and very fast)
    public TowerFireRate FireRate;
    //Targetting type of the tower
    public Targetting Targetting;

    //Actual numerical fire rate of the tower
    private float fireRate;
    //Time passed after the last projectile fired
    private float currTime;
    //List of possible targets detected by the tower
    private List<GameObject> targets;

    // Use this for initialization
    void Start()
    {
        targets = new List<GameObject>();

        //Set numerical fire rate
        switch (FireRate)
        {
            case TowerFireRate.Slow:
                fireRate = 1.2f;
                break;
            case TowerFireRate.Medium:
                fireRate = 0.9f;
                break;
            case TowerFireRate.Fast:
                fireRate = 0.5f;
                break;
            case TowerFireRate.VeryFast:
                fireRate = 0.25f;
                break;
            case TowerFireRate.Null:
                //No actual fire rate
                fireRate = 9999;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Remove all null targets in the list
        targets.RemoveAll(x => x == null);

        FaceTarget();
        ModeOfTargetting();
        Shoot();
    }

    void Shoot()
    {
        //If there is a detected enemy ...
        if (DetectedEnemy != null)
        {
            //If the tower has recently fired...
            if (currTime > 0)
            {
                //Reduce current time till it is able to fire again
                currTime -= Time.deltaTime;
            }
            //Else if the tower is able to fire again...
            else if (currTime <= 0)
            {
                //Set the current time to the numerical fire rate
                currTime = fireRate;
                //Then spawn the projectile of the tower
                GameObject projectile = (GameObject)Instantiate(Projectile);
                //And setup the projectiles target, damage, and position
                projectile.GetComponent<Projectile>().SetupProjectile(DetectedEnemy, Damage, transform.position);
            }
        }
    }

    void FaceTarget()
    {
        //If there is a detected enemy ...
        if (DetectedEnemy != null)
        {
            //Get the direction from the tower to the enemy detected
            Quaternion LookAt = Quaternion.LookRotation(DetectedEnemy.transform.position - transform.position);
            //Only allow rotation in the y axis
            LookAt.x = 0;
            LookAt.z = 0;
            //Lerp the rotation from the original rotation to the new direction
            transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, 5 * Time.deltaTime);
        }
    }

    void ModeOfTargetting()
    {
        //If there are possible targets ...
        if (targets.Count > 0)
        {
            //Check the targetting mode
            switch (Targetting)
            {
                //If the targetting is set to first...
                case Targetting.First:
                    //Get the first target detected by the tower
                    DetectedEnemy = targets[0];
                    break;
                //Else if the targetting is set to Last...
                case Targetting.Last:
                    //Get the last target detected by the tower
                    DetectedEnemy = targets[targets.Count - 1];
                    break;
                //Else if the targeting is set to the lowest hp...
                case Targetting.LowestHP:
                    //Start with the first target detected
                    GameObject tmpTarget = targets[0];
                    //Then compare the initial target's health to every other possible targets detected
                    foreach (GameObject target in targets)
                    {
                        if (target && tmpTarget)
                            if (target.GetComponent<EnemyStats>().HP < tmpTarget.GetComponent<EnemyStats>().HP)
                            {
                                tmpTarget = target;
                            }
                    }
                    //then set the new target
                    DetectedEnemy = tmpTarget;
                    break;
            }
        }
        //Else if there is no detected enemy
        else
        {
            DetectedEnemy = null;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //If enemy enters range
        if (other.CompareTag("Enemy"))
        {
            Type type = other.GetComponent<EnemyType>().Type;

            //Check if the types that the tower can target is the same as
            //the enemy detected, then add to possible targets list
            for (int i = 0; i < TargetableType.Count; i++)
            {
                if (type == TargetableType[i])
                {
                    targets.Add(other.gameObject);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //If enemy exits range
        if (other.CompareTag("Enemy"))
        {
            Type type = other.GetComponent<EnemyType>().Type;

            //Find the enemy that exited the range of the tower
            //and remove it from the possible targets list
            for (int i = 0; i < TargetableType.Count; i++)
            {
                if (type == TargetableType[i])
                {
                    targets.Remove(targets.Find(x => x == other.gameObject));
                }
            }
        }
    }
}
