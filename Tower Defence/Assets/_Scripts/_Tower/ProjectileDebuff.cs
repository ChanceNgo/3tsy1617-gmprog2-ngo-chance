﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDebuff : MonoBehaviour {

    public GameObject Debuff;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnDestroy () {

        Collider[] col = Physics.OverlapSphere(transform.position, 1.5f);
        foreach (Collider hit in col)
        {
            bool applyDebuff = true;

            if (hit.CompareTag("Enemy"))
            {
                foreach (Transform child in hit.transform)
                {
                    if (child.name.ToLower().Contains(Debuff.name.ToLower()))
                    {
                        applyDebuff = false;
                        child.GetComponent<Debuff>().remainingDuration = child.GetComponent<Debuff>().Duration;
                        break;
                    }
                }
                if (applyDebuff)
                {
                    GameObject debuff = (GameObject)Instantiate(Debuff);
                    debuff.transform.SetParent(hit.transform);
                    debuff.GetComponent<Debuff>().Target = GetComponent<Projectile>().Target;
                }
            }
        }
    }
}
