﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public enum Type
    {
        Splash,
        Single
    }

    public Type ProjectileType;
    public float ProjectileSpeed;
    public GameObject Target;
    public Vector2 Damage;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null)
        {
            //Get the distance between the projectile
            float dist = Vector3.Distance(transform.position, Target.transform.position);
            //If the distance is less than 0.5f
            if (dist > 0.5f)
            {
                //Lerp towards the target
                transform.position = Vector3.Lerp(transform.position, Target.transform.position, ProjectileSpeed * Time.deltaTime);
            }
            //If the distance is less than 0.5f
            else
            {
                //Generate a random number based on the damage values
                int dmg = Mathf.CeilToInt(Random.Range(Damage.x, Damage.y));

                //If the projectile type is splash
                if (ProjectileType == Type.Splash)
                {
                    //Get the enemies within a certain radius of the projectile
                    Collider[] col = Physics.OverlapSphere(transform.position, 1.5f);
                    foreach (Collider hit in col)
                    {
                        if (hit.CompareTag("Enemy"))
                        {
                            //Then apply the damage to each of the enemies hit in the radius
                            hit.GetComponent<EnemyStats>().ReceiveDamage(dmg);
                        }
                    }
                }
                //Else if the projectile type is single
                else if (ProjectileType == Type.Single)
                {
                    //Apply the damage to the target
                    Target.GetComponent<EnemyStats>().ReceiveDamage(dmg);
                }
                //Then destroy the projectile
                Destroy(gameObject);
            }
        }
        //If there is no target or the target is dead, destroy the projectile
        else
            Destroy(gameObject);
    }

    public void SetupProjectile(GameObject target, Vector2 damage, Vector3 startPos)
    {
        Target = target;
        Damage = damage;
        transform.position = startPos;
    }
}
