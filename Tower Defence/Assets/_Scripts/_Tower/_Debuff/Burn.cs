﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burn : Debuff
{
    public int Damage;

    // Use this for initialization
    void Start()
    {
        remainingDuration = Duration;
        StartCoroutine(BurnDamage());
    }

    // Update is called once per frame
    void Update()
    {
        remainingDuration -= Time.deltaTime;
    }

    IEnumerator BurnDamage()
    {
        while (remainingDuration > 0)
        {
            yield return new WaitForSeconds(0.5f);
            if (Target != null)
                Target.GetComponent<EnemyStats>().ReceiveDamage(Damage);
        }
        StopCoroutine(BurnDamage());
        Destroy(gameObject);
    }
}
