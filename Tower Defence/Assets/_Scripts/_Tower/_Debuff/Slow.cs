﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Slow : Debuff {

    [Range(0, 100)]
    public int SlowPercent;
    float origSpeed;

    // Use this for initialization
    void Start()
    {
        origSpeed = Target.GetComponent<NavMeshAgent>().speed;
        remainingDuration = Duration;
        StartCoroutine(SlowDown());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator SlowDown()
    {
        if (Target != null)
        {
            Target.GetComponent<NavMeshAgent>().speed = origSpeed - (origSpeed * SlowPercent/100);
            yield return new WaitForSeconds(Duration);
            if (Target != null)
            {
                Target.GetComponent<NavMeshAgent>().speed = origSpeed;
                StopCoroutine(SlowDown());
                Destroy(gameObject);
            }
        }
    }
}
