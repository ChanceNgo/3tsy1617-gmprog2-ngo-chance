﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuff : MonoBehaviour {

    public GameObject Target;
    public float Duration;

    public float remainingDuration { get; set; }
}
