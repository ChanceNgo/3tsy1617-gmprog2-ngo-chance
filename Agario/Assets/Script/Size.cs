﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Size : MonoBehaviour {

    public float CurrentSize;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        SizeUpdate();

        if (tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                AddSize(0.25f);
            }
            else if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                RemoveSize(0.25f);
            }
        }
    }

    void SizeUpdate()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(CurrentSize * 0.75f, CurrentSize * 0.75f), Time.deltaTime);

    }

    public void AddSize(float val)
    {
        CurrentSize += val;
    }

    public void RemoveSize(float val)
    {
        CurrentSize -= val;
        if (CurrentSize < 0)
            CurrentSize = 0.1f;
    }
}
