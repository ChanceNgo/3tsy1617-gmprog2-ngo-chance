﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyState2 : MonoBehaviour
{
    public enum AIState
    {
        Chase,
        Eat
    }

    public GameObject Player;

    private AIState currentState;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentState == AIState.Chase)
            ChaseState();
        else if (currentState == AIState.Eat)
            EatState();
    }

    void ChaseState()
    {
        //bool isDead = transform.position.x > 5; // This works
        //if (isDead)
        float AISize = GetComponent<Size>().CurrentSize;
        float PlayerSize = Player.GetComponent<Size>().CurrentSize;

        
        // Should be bigger than player
        // bool isLarger = AISize > PlayerSize
        // if (AISize > PlayerSize) // if (transform.scale.x > Player.transform.scale.x)
        // Close to the player
        float distance = Vector3.Distance(Player.transform.position, this.transform.position);
        if (distance < ChaseDistance && AISize > PlayerSize)
        {

        }
        //      transform.position += .....
        //      transform.Translate(....)
        //      transform.position = Vector3.MoveTowards(.....)
        // else
        //{
        //     currentState = AIState.Eat;
        //}
    }

    void EatState()
    {
        // Find closest food

        // Collider[] validFood = Physics.OverlapSphere -> array
        // Loop through each element
        //      targetFood, largestDistance
        //      foreach (.....)
        //          float currentDistance = Vector3.Distance(me, currentFood)
        //          if (currentDistance < largestDistance) {
        //              currentDistance = largetDistance
        //              targetFood = currentFood
        //          }

        // Compare distance

        // Use distance between this.transform & food.transform

        // Move to closest food
        //      transform.position += .....
        //      transform.Translate(....)
        //      transform.position = Vector3.MoveTowards(.....)


        // Review on variable scope
        //for(int i = 0; i < 5; i++)
        //{
        //    float largestDistance = 0.0f;
        //}

    }
}
