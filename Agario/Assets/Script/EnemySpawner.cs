﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject EnemyPrefab;

    public int MaxFoodSpawned;
    public float SpawnRate;
    public Vector2 MapBoundary;

    private List<GameObject> EnemySpawned;
    private float currentTime;

    // Use this for initialization
    void Start()
    {
        EnemySpawned = new List<GameObject>();

        for (int i = 0; i < 5; i++)
        {
            Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
            GameObject food = (GameObject)Instantiate(EnemyPrefab, randPos, Quaternion.identity);
            EnemySpawned.Add(food);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (EnemySpawned.Count < MaxFoodSpawned)
        {
            if (currentTime > 0)
            {
                currentTime -= Time.deltaTime;
            }
            else if (currentTime <= 0)
            {
                Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
                GameObject food = (GameObject)Instantiate(EnemyPrefab, randPos, Quaternion.identity);
                EnemySpawned.Add(food);
                currentTime = SpawnRate;
            }
        }
    }
}
