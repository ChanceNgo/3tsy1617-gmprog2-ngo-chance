﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Food")
        {
            GetComponent<Size>().AddSize(other.GetComponent<Size>().CurrentSize);
            Destroy(other.gameObject);
        }
        else if(other.tag == "Player")
        {
            float mySize = GetComponent<Size>().CurrentSize;
            float otherSize = other.GetComponent<Size>().CurrentSize;

            if (mySize > otherSize)
            {
                GetComponent<Size>().AddSize(otherSize / 2);
                Destroy(other.gameObject);
            }
        }
    }
}
