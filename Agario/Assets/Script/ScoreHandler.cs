﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour {

    public GameObject Player;
    public Text ScoreUI;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Player)
        {
            float playerSize = Player.GetComponent<Size>().CurrentSize;
            ScoreUI.text = "Score : " + Mathf.CeilToInt((playerSize));
        }
    }
}
