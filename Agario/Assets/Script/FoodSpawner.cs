﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour {

    public GameObject FoodPrefab;

    public int MaxFoodSpawned;
    public float SpawnRate;
    public Vector2 MapBoundary;

    private List<GameObject> FoodSpawned;
    private float currentTime;

	// Use this for initialization
	void Start () {
        FoodSpawned = new List<GameObject>();

        for(int i = 0; i < 30; i++)
        {
            Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
            GameObject food = (GameObject)Instantiate(FoodPrefab, randPos, Quaternion.identity);
            FoodSpawned.Add(food);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (FoodSpawned.Count < MaxFoodSpawned)
        {
            if(currentTime > 0)
            {
                currentTime -= Time.deltaTime;
            }
            else if(currentTime <= 0)
            {
                Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
                GameObject food = (GameObject)Instantiate(FoodPrefab, randPos, Quaternion.identity);
                FoodSpawned.Add(food);
                currentTime = SpawnRate;
            }
        }
	}
}
